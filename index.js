const express = require('express')
const app = express()
const dbConnect = require('./utils/database')
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const userRoutes = require('./routes/user')
const passwordRoute = require('./routes/resetPassword')
const postRoutes = require('./routes/article')

app.get('/', (req,res) => {
    res.send('Home page!')
})
app.use('/user', userRoutes )
app.use('/', passwordRoute )
app.use('/', postRoutes )

app.listen(4000, () => {
    console.log('Server started on port 4000')
    dbConnect()
})