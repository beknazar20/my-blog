const mongoose = require('mongoose');

const articleSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,

    title: {
        type:String,
        required:true
    },

    content: {
        type: String,
        required:true
    },
    image: String,

    date: {
        type:Date,
        default: Date.now
    },

    authorId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
    },
    comments: [
        {
            id_author: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'User',
                required: true
            },
            content: String,
            date: {
                type: Date,
                default: Date.now
            }
        }
    ]

})
 

module.exports = mongoose.model('article', articleSchema)