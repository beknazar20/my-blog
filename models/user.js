const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { 
        type:String,
        required:true,
        minLength: 2,
        maxLength: 24
    },
    secondName: { 
        type:String,
        required:true,
        minLength: 2,
        maxLength: 24
    },
    email: { 
        type:String,
        unique:true,
        required:true
    },
    password: { 
        type:String,
        required:true,
        minLength: 6,
        maxLength: 56
    },
    userAvatar: String,
    resetLink:String
})
module.exports = mongoose.model('user', userSchema)