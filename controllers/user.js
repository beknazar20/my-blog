const mongoose = require('mongoose')
const User = require('../models/user')
const { hash, compare } = require('bcrypt')
const jwt = require('jsonwebtoken')
const emailConfirm = require('../utils/nodemailer')

exports.signUp = (req,res) => {
    const userAvatar = req.file.path;
    const { name, secondName, email, password, password_repeat } = req.body;
 
    User.findOne({email: email}).exec()
        .then(user => {
            if(user){
                return res.status(422).json({
                    message: 'Email exists!'
                })
            }else{
                if(password !== password_repeat){
                   return res.json({
                        status: 'password mismached!'
                    })
                }

                jwt.sign({name, secondName, email, password, password_repeat,userAvatar},process.env.JWT_SECRET, (err,content) => {
                    const options = {
                        from: '<beku.00000@gmail.com>',
                        to: email,
                        subject: 'This is your email activate token',
                        text: `Hello ${name } ${secondName}`,
                        html: `<a href="${process.env.LOC}/${content}"> ${content} </a>`
                    }
                    emailConfirm(options)
                })
               return res.json({
                    message: 'Sended email confreimation fro your email'
                })
               
            }
        })
    }

exports.emailConfirm = (req,res,next) => {
    const { token } = req.body;
    
    if(token){
        jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
            if(err){
                return res.json({
                    error: err
                })
            }
            const {name, secondName, email, password, password_repeat, userAvatar } = decodedToken;
            hash(password, 10 ,(err,hashedPassword)=> {
               console.log(err)
                const user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    name,
                    secondName,
                    email,
                    password: hashedPassword,
                    userAvatar: userAvatar
                    
                })
                user.save()
                .then(result => {
                    res.status(201).json({
                        message: "User created!!"
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        error: err
                    })
                })
            })  
        
        })
    }  
}

exports.userLogin = (req,res,next) => {
    const { email, password } = req.body;
    User.find({email: email})
        .exec()
        .then(user => {
            if(user.length < 1){
                return res.status(401).json({
                    message: 'Auth failed!! User with this Email not funded'
                })
            }
            compare(password, user[0].password, (err,result)=>{
                if(result){
                    const token = jwt.sign({
                      email: user[0].email,
                      userId: user[0]._id
                    }, process.env.JWT_SECRET);
                    return res.status(200).json({
                        message: 'Auth successful!!',
                        token
                    })
                }
                return res.status(401).json({
                    message: 'Auth failed!! Password not correct!'
                })
            })
        })
}

exports.changeUserData = (req,res,next) => {
    const id = req.params.id;
    const { newName,newSecondName } = req.body;
    User.updateOne({_id: id}, {$set: {name: newName,secondName:newSecondName }})
        .then(result => {
            return res.status(201).json({
                message: 'Changed your date!'
            })
        })
}