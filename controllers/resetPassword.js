const User = require('../models/user')
const jwt = require('jsonwebtoken')
const resetEmail = require('../utils/nodemailer')
const { hash } = require('bcrypt')
exports.forgotPassword = (req,res)=> {
    const { email } = req.body;
    User.findOne({email: email}, (err, user) => {
        if(err || !user){
            return res.status(404).json({
                error: 'User not found!!'
            })
        }
        const token = jwt.sign({_id: user._id},process.env.JWT_SECRET,{expiresIn:'20m'})

        const options = {
            from: '<beku.00000@gmail.com>',
            to: email,
            subject: "Hello this is your password reset token", 
            html: `<a href="${token}">${token}</a>`

        }
        return user.updateOne({resetLink: token},(err,result) => {
            if(err){
                res.json('Reset password link error')
            }else{
                resetEmail(options)
                res.status(201).json({
                    message: 'Sended you Email '
                })
            }
        })
       
    })
    }


    exports.resetPassword = async (req,res)=> {
        const { resetLink,newPassword } = req.body;
        const hashedPassword = await hash(newPassword,10)
        if(resetLink){
        jwt.verify(resetLink, process.env.JWT_SECRET, (err,decodedData) => {
            if(err){
                return res.json ({
                    err: 'ERRROR in your Link'
                })
            }
            User.findOne({resetLink: resetLink},(err,user)=> {
              if(err || !user){
                  return res.status(404).json('Error in resetLink55')
              }     
            })
            .then(user => {
                console.log('>>>>>>>>>>>>',user)
            })
            .catch(err => {
                console.log(err)
            })
        })
    }
    User.updateOne({resetLink: resetLink}, {"$set": {"resetLink": "", "password": hashedPassword} })
        .then(result => {
            res.json({message: 'Yout password changed!!'})
        })
        .catch(err => {
            res.send(err)
        })
    }