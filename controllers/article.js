const Article = require('../models/article')
const fs = require('fs')
const { postValidation } = require('../utils/validations')
const User = require('../models/user')
const mongoose = require('mongoose')
exports.addNewPost = (req,res,next)=> {
    const { error } = postValidation(req.body); 
    if (error) return res.status(400).send(error.details[0].message);
  
 const imagePath = req.file.path;
 console.log(imagePath)
 const { title, content } = req.body
    const newPost = new Article({
    _id: new mongoose.Types.ObjectId(),
    title,
    content,
    image: imagePath,
    authorId: req.user.userId
})
newPost.save()  
    .then(result => {
        console.log(result)
        return res.json({result})
    })
    .catch(err => {
        console.log(err)
        return res.json({err})
    })
}
exports.getMyPosts = (req,res,next)=> {
    Article.find({authorId: req.user.userId}, (err,result) => {
        if(err){
            console.log(err)
        }else{
            res.status(201).json({
                yourPosts: result
            })
        }
    })
}

exports.editPost = async (req,res,next)=> {
    const id = req.params.id;
    const newPostImage = req.file.path;
    const post = await Article.findOne({_id:mongoose.Types.ObjectId(id)})
    const imageUrl = post.image
   console.log(imageUrl)
  fs.unlink(imageUrl, (err,result) => {
      if(err) {
         console.log(err)
      }
      else {
        console.log('success')
      }
  })

    const { newTitle,newContent } = req.body;
    Article.findOneAndUpdate({_id: id}, {$set: {
        title: newTitle, 
        content:newContent,
        image:newPostImage 
    }})
        .then(result => {
            return res.status(201).json({
                res: result,
                message: 'Changed your date!'
            })
        })
}
exports.deletePost = (req,res,next)=> {
    const id = req.params.id;
    Article.findByIdAndDelete(id, (err,result)=> {
        if(err){
            return res.json({err})
        }else{
            return res.json({result})
        }
    })
}

exports.openPost = (req,res,next)=> {
    const id = req.params.id;
    Article.findById(id, (err,result)=> {
        if(err){
            res.json({err: 'Post with this ID not found!'})
        }else{
            res.status(201).json({
                post: result
            })
        }
    })
}
exports.writeComment = (req,res,next)=> {
    const id = req.params.id;
    const commentContent = req.body.comment
    Article.findOneAndUpdate({_id:id}, {$push: {comments: {id_author: req.user.userId, content: commentContent}}})
    .then(result => {
        res.json({result})
    })
    .catch(err => {
        console.log(err)
    })
}