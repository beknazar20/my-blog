const express = require('express')
const router = express()
const auth = require('../middleware/auth')

const multer = require('multer')


const storage = multer.diskStorage({
    destination: function(req,file,cb){
        cb(null, './uploads/')
    },
    filename: function(req,file,cb){
        cb( null, Math.random() * 10 + file.originalname)
    },
})

fileFilter = (req,file,cb)=> {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true)
    }else{
        cb(null, false)
    }
}

const upload = multer({
    storage,
    limits:{
        fileSize: 1024 * 1024 * 5
    },
    fileFilter
})
const UserRouteControllers = require('../controllers/user')
router.post('/signup', upload.single('userAvatar'), UserRouteControllers.signUp)
router.put('/emailActivate',  UserRouteControllers.emailConfirm)
router.post('/login', UserRouteControllers.userLogin)
router.put('/changeInfo/:id', UserRouteControllers.changeUserData)

module.exports = router