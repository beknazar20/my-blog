const express = require('express')
const router = express()
const password = require('../controllers/resetPassword')
router.post('/forgot-password', password.forgotPassword)
router.post('/resetPassword', password.resetPassword)

module.exports = router