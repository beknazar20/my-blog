const express = require('express')
const router = express()
const postRouts = require('../controllers/article')
const auth = require('../middleware/auth')
const multer = require('multer')


const storage = multer.diskStorage({
    destination: function(req,file,cb){
        cb(null, './uploads/postImage')
    },
    filename: function(req,file,cb){
        cb( null, Math.random() * 10 + file.originalname)
    },
})

fileFilter = (req,file,cb)=> {
    if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
        cb(null, true)
    }else{
        cb(null, false)
    }
}

const upload = multer({
    storage,
    limits:{
        fileSize: 1024 * 1024 * 5
    },
    fileFilter
})

router.post('/addNewPost',auth, upload.single('postImage'), postRouts.addNewPost )
router.put('/editPost/:id', auth, upload.single('newPostImage'), postRouts.editPost )
router.post('/deletePost/:id', auth, postRouts.deletePost)
router.get('/myPosts',auth, postRouts.getMyPosts)
router.get('/openPost/:id',auth, postRouts.openPost)
router.put('/writeComment/:id',auth, postRouts.writeComment)


module.exports = router