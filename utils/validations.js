const Joi = require('joi');


exports.userValidation = function userValidation(user){

    const schema = Joi.object({
        name: Joi.string().min(2).max(24).required(),
        secondName: Joi.string().min(2).max(24).required(),
        email: Joi.string().max(24).required().email(),
        password: Joi.string().max(56).required().min(6),
        userAvatar: Joi.string()
        })
    return schema.validate(user)


}

exports.postValidation = function postValidation(article){

    const schema = Joi.object({
        title: Joi.string().min(2).max(24).required(),
        content: Joi.string().max(1024).required(),
        image: Joi.string()
        })
    return schema.validate(article)

}