const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config();
module.exports = function connetcDB(){
mongoose.connect(process.env.DB_CONNECT, {
     useNewUrlParser: true, 
     useUnifiedTopology: true,
     useCreateIndex:true,
     useFindAndModify:false },
()=>console.log("Connected to db"));
}